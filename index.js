const password = "qLOUjLAV7KYSceYT"

const { MongoClient, ServerApiVersion } = require('mongodb');
const uri = "mongodb+srv://craigsworth:"+password+"@cluster0.wtr3in8.mongodb.net/?retryWrites=true&w=majority";
const client = new MongoClient(uri, 
	{ useNewUrlParser: true, 
	useUnifiedTopology: true, 
	serverApi: ServerApiVersion.v1 
	});

console.log("Testing connection ...")

client.connect(err => {
	//const collection = client.db("test").collection("devices");
	
	// perform actions on the collection object
	var dbo = client.db("sample_mflix");
	console.log("Connected successfully")
  	dbo.collection("users").find({}).toArray(function(err, result) {
	    if (err) throw err;

	    console.log("Users:");
	    console.log(result);
	    client.close();
		console.log("Disconnected successfully")
	  });
});